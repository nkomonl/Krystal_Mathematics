
		
		window.addEventListener('load', function(event) {
		   Canvas(); 
		});
			
		function Canvas(){

	//variables//////////////////////////////////////////////////////variables
		var bg = new Image();
		
		bg.src = "str.jpg";
		
		var balls = [];
		
		var iSprDir = 0; // initial ball direction
		
		var iBallSpeed = 10; // initial ball speed

		var canvas = document.getElementById('canvas');
		
		var ctx = canvas.getContext('2d');
		
		canvas.width = document.documentElement.clientWidth*0.98;
		
		canvas.height = document.documentElement.clientHeight*0.75;
		
		var cW = ctx.canvas.width, cH = ctx.canvas.height;
		
		var background = new Background();	 
		
		var randomnumber=Math.floor(Math.random()*50);
//player sprites work 
		var pTiles = {loaded: false,image: new Image(),tileWidth: 64,tileHeight: 64};
			  
		var mySprite = {x: 200,	y: 200,	width: 64,height: 64,speed: 200,state: 3};
 
		var shipSprites2 = {loaded: false,image: new Image(),tileWidth: 64,	tileHeight: 64};
		
		var item2 = {x: canvas.width*1.5,y: 200,width: 64,height: 64,speed: 200,state: 0};
		
		var shipSprites3 = {loaded: false,image: new Image(),tileWidth: 86,	tileHeight: 86};
		
		var mySprite3 = {x: canvas.width*1.7,y: 200,width: 86,height: 86,speed: 200,state: 0};
		
		var shipSprites4 = {loaded: false,image: new Image(),tileWidth: 128,tileHeight: 128};
		
		var mySprite4 = {x: canvas.width*1.4,y: canvas.height*.8,width: 128,height: 128,speed: 200,state: 0};
		
		var shipSprites5 = {loaded: false,image: new Image(),tileWidth: 48,	tileHeight: 48};
		
		var mySprite5 = {x: canvas.width*1.5,y: canvas.height*.2,width: 48,height: 48,speed: 200,state: 0};
		
		var shipSprites6 = {loaded: false,image: new Image(),tileWidth: 128,tileHeight: 128};
		
		var item = {x: canvas.width*1.1,y: canvas.height*.6,width: 128,	height: 128,speed: 200,	state: 0};
		
		var shipSprites7 = {loaded: false,image: new Image(),tileWidth: 86,tileHeight: 86};
		
		var mySprite7 = {	x: canvas.width*1.2,y: 200,	width: 86,height: 86,speed: 200,state: 0};

		var shipSprites8 = {loaded: false,image: new Image(),tileWidth: 86,tileHeight: 86};
		
		var mySprite8 = {x: canvas.width*1.7,y: 100,width: 86,height: 86,speed: 200,state: 0};
//variable of the pointcounter.
		var itemCounter = 0;
		
		var keysDown = {};
		//**************************************************Boom+life+total score***************************************Boom+life+total score******************************
		
		var BoomImage;
		var Boom = [];
		var Life = 100;
		var TotalScore=0;
		
// objects ////////////////////////////////////////objects//////////////////////////////////////////////objects/////////////////////////////////
				
		function Ball(x, y, w, h, speed, image) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
			this.speed = speed;
			this.image = image;
			}
		function BoomNow(x, y, w, h, sprite, image) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
			this.sprite = sprite;
			this.image = image;
		}

		
//update and draw///////////////////////////////////////////////update and draw/////////////////////////update and draw//////////////////////////
		
		
	updateAndDraw =function() {

				background.render();
				drawInstruction();
				drawItemCounter();
				drawWiningScore();
				DrawPlayer();
				drawLife();
				drawTotalScore();
				DrawObstacle1();
				DrawObstacle3();
				DrawObstacle4();
				DrawObstacle5();
				DrawObstacle6();
				DrawObstacle7();
				DrawObstacle8();		
			//bullet
				if (balls.length > 0) {
					for (var key in balls) {
						if (balls[key] != undefined) {
							ctx.drawImage(balls[key].image, balls[key].x, balls[key].y);
							balls[key].x += balls[key].speed;

							if (balls[key].x > canvas.width) {
								delete balls[key];
							}
						}
					}
				}
		
		 // draw explosions
        if (Boom.length > 0) {
            for (var key in Boom) {
                if (Boom[key] != undefined) {
                    // display explosion sprites
                    ctx.drawImage(Boom[key].image, Boom[key].sprite*Boom[key].w, 0, Boom[key].w, Boom[key].h,
					Boom[key].x - Boom[key].w/2, Boom[key].y - Boom[key].h/2, Boom[key].w, Boom[key].h);
                    Boom[key].sprite++;

                    // remove an explosion object when it expires
                    if (Boom[key].sprite > 10) {
                        delete Boom[key];
                    }
                }
            }
        }
		
		}
		
		
	//update/////////////////////////////////////////////////////////////////////update//////////////////////////////////////////update/////
	update = function(mod) {

			if (37 in keysDown) {
			if(mySprite.x>=-32)
			 {   mySprite.state = 1; //left
				mySprite.x -= mySprite.speed * mod;
			}}
			if (38 in keysDown) {
			 if(mySprite.y>=-32) 
			  {mySprite.state = 0; //up
				mySprite.y -= mySprite.speed * mod;
			}}
			
			
			 if (39 in keysDown) {
			if(mySprite.x<= cW*.9){
				mySprite.state = 3; //right
				mySprite.x += mySprite.speed * mod;
			}}
		   
			if (40 in keysDown) {
			if(mySprite.y<= cH*.9){
				mySprite.state = 2; //down
				mySprite.y += mySprite.speed * mod;
			}}
			
	if(randomnumber==itemCounter)
			{
		alert('you have won!!! click "ok" to PLAY again!!!');
		itemCounter=0;
					Life=100;
					randomnumber=Math.floor(Math.random()*50);
							}

	if(0>Life)
			{
		alert('you have LOST!!! click "ok" to PLAY again!!!');
		//itemCounter++;
					location.reload();
					
					}
	if(itemCounter<-100){
				alert('you have a score below\n\n -100'+'\n\nyou have lost the Game');	
				location.reload();
			}
	if(randomnumber<itemCounter)
			{
		alert('you have lost 10 life points!!!\n\n Your score must not be above the Green value click "ok" to PLAY again!!!');
			itemCounter=0;
					Life-=10;
					
					}
			
			
			if (balls.length > 0) {
				for (var key in balls) {
					if (balls[key] != undefined) {
					if (
				balls[key].x < item.x + 128 &&
				balls[key].x + 32 > item.x &&
				balls[key].y < item.y + 128 &&
				balls[key].y + 32> item.y
			) 
			{
				
				item.y = Math.random() * canvas.height*.8;
				itemCounter +=1;//change this to change the amount of points/rock.
				 
				 Boom.push(new BoomNow(balls[key].x+(.05*cW), balls[key].y, 120, 120, 0, BoomImage));
				//mySprite.x, mySprite.y, 32, 32, iBallSpeed, oBallImage 
				//DrawBoomNow();
				
				 item.x = 1.2* canvas.width;
				  delete balls[key];
				  TotalScore+=5;
			}}}
			
			}
			
				if (balls.length > 0) {
				for (var key in balls) {
					if (balls[key] != undefined) {
					if (
				balls[key].x < item2.x + 64 &&
				balls[key].x + 32 > item2.x &&
				balls[key].y < item2.y + 64 &&
				balls[key].y + 32> item2.y
			) 
			{
			Boom.push(new BoomNow(balls[key].x+(.05*cW), balls[key].y, 120, 120, 0, BoomImage));
				item2.x = 1.2* canvas.width;
				item2.y = Math.random() * canvas.height*.8;
				itemCounter +=1;
				 delete balls[key];
				 TotalScore+=5;
		}}}
		}

				if (balls.length > 0) {
				for (var key in balls) {
					if (balls[key] != undefined) {
					if (
				balls[key].x < mySprite5.x + 64 &&
				balls[key].x + 32 > mySprite5.x &&
				balls[key].y < mySprite5.y + 64 &&
				balls[key].y + 32> mySprite5.y

			) 
			{
			Boom.push(new BoomNow(balls[key].x+(.05*cW), balls[key].y, 120, 120, 0, BoomImage));
				mySprite5.x = 1.2* canvas.width;
				mySprite5.y = Math.random() * canvas.height*.8;
				itemCounter +=15;
				 delete balls[key];
				 TotalScore+=5;
		}}}
				}

		if (balls.length > 0) {
				for (var key in balls) {
					if (balls[key] != undefined) {
					if (
				balls[key].x < mySprite3.x + 64 &&
				balls[key].x + 32 > mySprite3.x &&
				balls[key].y < mySprite3.y + 64 &&
				balls[key].y + 32> mySprite3.y

				) 
			{
				Boom.push(new BoomNow(balls[key].x+(.05*cW), balls[key].y, 120, 120, 0, BoomImage));
				mySprite3.x = 1.2* canvas.width;
				mySprite3.y = Math.random() * canvas.height*.8;
				itemCounter +=10;
				 delete balls[key];
				 TotalScore+=5;
		}}}
				}

		if (balls.length > 0) {
				for (var key in balls) {
					if (balls[key] != undefined) {
					if (
				balls[key].x < mySprite4.x + 64 &&
				balls[key].x + 32 > mySprite4.x &&
				balls[key].y < mySprite4.y + 64 &&
				balls[key].y + 32> mySprite4.y
		) 
			{
				Boom.push(new BoomNow(balls[key].x+(.05*cW), balls[key].y, 120, 120, 0, BoomImage));
				mySprite4.x = 1.2* canvas.width;
				mySprite4.y = Math.random() * canvas.height*.8;
				itemCounter +=2;
				 delete balls[key];
				 TotalScore+=5;
		}}}
		}
	
		if (balls.length > 0) {
				for (var key in balls) {
					if (balls[key] != undefined) {
					if (
				balls[key].x < mySprite7.x + 64 &&
				balls[key].x + 32 > mySprite7.x &&
				balls[key].y < mySprite7.y + 64 &&
				balls[key].y + 32> mySprite7.y

				) 
			{
				Boom.push(new BoomNow(balls[key].x+(.05*cW), balls[key].y, 120, 120, 0, BoomImage));
				mySprite7.x = 1.2* canvas.width;
				mySprite7.y = Math.random() * canvas.height*.8;
				itemCounter +=25; //this are the amount of points..
				 delete balls[key];
				 TotalScore+=5;
		}}}
				}
	
		if (balls.length > 0) {
				for (var key in balls) {
					if (balls[key] != undefined) {
					if (
				balls[key].x < mySprite8.x + 64 &&
				balls[key].x + 32 > mySprite8.x &&
				balls[key].y < mySprite8.y + 64 &&
				balls[key].y + 32> mySprite8.y

				) 
			{
				Boom.push(new BoomNow(balls[key].x+(.05*cW), balls[key].y, 120, 120, 0, BoomImage));
				mySprite8.x =1.2* canvas.width;
				mySprite8.y = Math.random() * canvas.height*.8;
				itemCounter -=25; //this are the amount of points..
				 delete balls[key];
				 TotalScore+=5;
		}}}
				}
				
				
				
	//relaunching obstacles
		if (mySprite5.x<0.01) //UFO
			{
				mySprite5.x = 1.2* canvas.width;
				mySprite5.y = Math.random() * canvas.height*.8;
				Life-=5;
		}


		if (mySprite4.x<0.01) //Large brown rock
			{
				mySprite4.x = 1.2* canvas.width;
				mySprite4.y = Math.random() * canvas.height*.8;
				Life-=5;
		}


		if (mySprite3.x<0.01)//small brown rock 
			{
				mySprite3.x = 1.2* canvas.width;
				mySprite3.y = Math.random() * canvas.height*.8;
			
				Life-=5;
		
		}


		if (item2.x<0.01) 
			{
				item2.x = 1.2* canvas.width;
				item2.y = Math.random() * canvas.height*.8;
			
				Life-=5;
		}


		if (item.x<0.01) //large roc yellow/green
			{
				item.x = 1.2* canvas.width;
				item.y = Math.random() * canvas.height*.8;
				Life-=5;
		}
	

		if (
				mySprite7.x<0.01) 
			{
				mySprite7.x = 1.1* canvas.width;
				mySprite7.y = Math.random() * canvas.height*.8;
				Life-=5;
		}
		
		if (
				mySprite8.x<0.01) 
			{
				mySprite8.x = 1.1* canvas.width;
				mySprite8.y = Math.random() * canvas.height*.8;
				
				Life-=5;
				
		}
		}
	///////////////////draw/////////////draw//////////////////////////////////draw/////////////////////////////////////draw
	
function drawWiningScore() {
		ctx.fillStyle = 'green';
		ctx.font = 'italic bold 40px sans-serif';
		ctx.textBaseline = 'top';
		ctx.fillText(randomnumber,cW*.9, cH*.9);
		}
		
//		drawLife();
//		drawTotalScore();

function drawLife(){ 
				ctx.font = '30pt Arial';
				ctx.fillStyle = 'orange';
				ctx.textBaseline = 'top';
				ctx.fillText("Life"+Life+" %", cW*.8, cH*.3);
				}
function drawTotalScore(){ 
				ctx.font = '30pt Arial';
				ctx.fillStyle = 'gold';
				ctx.textBaseline = 'top';
				ctx.fillText(" LevelScore" +TotalScore, cW*.75, cH*.1);
				}
function drawItemCounter(){ 
				ctx.font = '32pt Arial';
				ctx.fillStyle = 'red';
				ctx.textBaseline = 'top';
				ctx.fillText(itemCounter, 10, 10);}
function drawInstruction(){
				ctx.fillStyle = 'white';
				ctx.font = 'italic bold 20px sans-serif';
				ctx.textBaseline = 'bottom';
				ctx.fillText("wanna win? Get a score of:", cW*.7, cH*.9);
		}
	
function winScore(){	
				var choose1=randnum(0,10);
				document.math.firstnum.value=choose1;
				this.render=function()
				{ctx.font = '32pt Arial';
				ctx.fillStyle = 'red';
				ctx.textBaseline = 'top';
				ctx.fillText(choose1, 500, 10);
				}
			}	
function DrawPlayer(){
		if (pTiles.loaded) {
			ctx.drawImage(
				pTiles.image,
				mySprite.state * pTiles.tileWidth,
				0, 
				mySprite.width,
				mySprite.height,
				mySprite.x,
				mySprite.y,
				mySprite.width,
				mySprite.height
			);
		}
		}
		
function DrawObstacle1(){
		
		if (shipSprites2.loaded) {
        if(item2.x>cW*.001){
		ctx.drawImage(
            shipSprites2.image,
            item2.state * shipSprites2.tileWidth,
            0, 
            item2.width,
            item2.height,
            item2.x-=1.5,
            item2.y,
            item2.width,
            item2.height

        );
		
		}}
		}
		
		
function DrawObstacle3(){if (shipSprites3.loaded) {
		
        ctx.drawImage(
            shipSprites3.image,
            mySprite3.state * shipSprites3.tileWidth,
            0, 
            mySprite3.width,
            mySprite3.height,
            mySprite3.x-=1.6,
            mySprite3.y,
            mySprite3.width,
            mySprite3.height

        );
		}}
		
function DrawObstacle4(){
		
		if (shipSprites4.loaded) {
        if(mySprite4.x>cW*.001){
		ctx.drawImage(
            shipSprites4.image,
            mySprite4.state * shipSprites4.tileWidth,
            0, 
            mySprite4.width,
            mySprite4.height,
            mySprite4.x-=1.7,
            mySprite4.y,
            mySprite4.width,
            mySprite4.height

        );
		}}}
		
function DrawObstacle5(){if (shipSprites5.loaded) {
        
		ctx.drawImage(
            shipSprites5.image,
            mySprite5.state * shipSprites5.tileWidth,
            0, 
            mySprite5.width,
            mySprite5.height,
            mySprite5.x-=2,
            mySprite5.y,
            mySprite5.width,
            mySprite5.height

        );
		}
		}
		
function DrawObstacle6(){
			if (shipSprites6.loaded) {
        
		ctx.drawImage(
            shipSprites6.image,
            item.state * shipSprites6.tileWidth,
            0, 
            item.width,
            item.height,
            item.x-=1.5,
            item.y,
            item.width,
            item.height

        );
		
		}
		}
function DrawObstacle7(){
		
			
		if (shipSprites7.loaded) {
        if(mySprite7.x>cW*.001){
		ctx.drawImage(
            shipSprites7.image,
            mySprite7.state * shipSprites7.tileWidth,
            0, 
            mySprite7.width,
            mySprite7.height,
            mySprite7.x-=1.7,
            mySprite7.y,
            mySprite7.width,
            mySprite7.height

        );
		}}
		}
function DrawObstacle8(){
		
		if (shipSprites8.loaded) {
		
        ctx.drawImage(
            shipSprites8.image,
            mySprite8.state * shipSprites8.tileWidth,
            0, 
            mySprite8.width,
            mySprite8.height,
            mySprite8.x-=1.6,
            mySprite8.y,
            mySprite8.width,
            mySprite8.height

        );
		}
		}
		
	//background
	function Background(){
			this.x = 0, this.y = 0, this.w = bg.width, this.h = bg.height;
			this.render = function(){
				ctx.drawImage(bg, this.x-=1.5, 0);
				if(this.x <= -2000){
					this.x = 0;
				}
			}
		}
		
		
		
		
// initialization///////////////////////////////////////////////////////initialization///////////////////////////////////////////initialization///

		pTiles.image.onload = function() {  pTiles.loaded = true;}
			 
			pTiles.image.src = 'ship.png';
			
		shipSprites2.image.onload = function() {
		   shipSprites2.loaded = true;
		   }
		shipSprites2.image.src = 'roc2.png';

		 
		shipSprites3.image.onload = function() {
		   shipSprites3.loaded = true;
		   }
		shipSprites3.image.src = 'roc3.png';

		 
		shipSprites4.image.onload = function() {
		   shipSprites4.loaded = true;
		   }
		shipSprites4.image.src = 'roc4.png';

		 
		shipSprites5.image.onload = function() {
		   shipSprites5.loaded = true;
		   }
		shipSprites5.image.src = 'ufo.png';
			
			
			
		shipSprites6.image.onload = function() {
		   shipSprites6.loaded = true;
		   }
		shipSprites6.image.src = 'roc1.png';
		
		shipSprites7.image.onload = function() {
		   shipSprites7.loaded = true;
		   }
		shipSprites7.image.src = 'roc6.png';
	
		shipSprites8.image.onload = function() {
		   shipSprites8.loaded = true;
		   }
		shipSprites8.image.src = 'roc5.png';
		
		// initialization of Boom image
   
	var BoomImage = new Image();
    BoomImage.src = 'boom.png';
    BoomImage.onload = function() { }
  	// initialization of fireball image
		var oBallImage = new Image();
		oBallImage.src = 'fireball.png';
		oBallImage.onload = function() { }
		
		
	// initialization of empty ball (bullet)////////////////////
	
	
	$(function(){
	


		$(window).keydown(function(event){ // keyboard alerts///////////////////////////////////////////
			switch (event.keyCode) {
				case 32: // 'spacebar' key
					balls.push(new Ball(mySprite.x, mySprite.y, 32, 32, iBallSpeed, oBallImage));
				break;
			}
		});


	});

	
window.addEventListener('keydown', function(e) {
				keysDown[e.keyCode] = true;
			});
window.addEventListener('keyup', function(e) {
				delete keysDown[e.keyCode];
				mySprite.state = 3;
			});


	
 
function main() {
    update((Date.now() - time) / 1000);
    updateAndDraw();
    time = Date.now();
}
 
var time = Date.now();
setInterval(main, 20);

   
}
function HelpMenu(){alert
('Welcome to the help note\n\n-To win game life, make a red score(top left)\n\n that is equal to the green\n\n number(bottom right of screen)\n\n- If obstacles pass away, they \n\n will make your redscore reduce\n\n -At ascore of-100, it is Game Over' );}

function Pause(){alert('PAUSED...CLICK OK TO CONTINUE');}
			